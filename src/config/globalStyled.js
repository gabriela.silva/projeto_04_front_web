import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`

    * {
        margin: 0;
        padding: 0;
        outline: 0;
        
    }

    #root {
        display:block;
        flex-direction:column;
        height: 100%;
        width: 100%;
        position: relative;
    }

`

export default GlobalStyle
