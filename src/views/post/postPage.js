import React, { useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  Grid,
  CssBaseline,
  Box,
  Typography,
  Button,
  Card,
  CardContent,
  CardMedia,
} from '@material-ui/core'
import DialogModal from '../../components/dialog'
import {
  create as createPosts,
  getAll as getAllPosts,
} from '../../store/post/action'
import {
  getAll as getMusicos
} from '../../store/musicos/action'
import FormPost from '../../components/post/form'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    paddingTop: '40px',
    flexDirection: 'column',
    width: '100%',
    marginLeft: '10px'
  },
  title: {
    display: 'flex',
    justifyContent: 'space-between',
    paddingBottom: '30px',
    paddingRight: '40px',
    paddingLeft: '40px',
    color: '#402672'
  },
  button: {
    backgroundColor: '#402672'
  },
  cardgrid: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    padding: '5px',
    margin: '5px'
  },
  cardmusico: {
    width: '35%',
    margin: '5px'
  },
  cardMedia: {
    paddingTop: '56.25%' // 16:9
  },
  body: {
    marginTop: '10px',
    fontSize: '16px',
    color: '#402672'
  }
}))

const PostCreate = () => {
  const classes = useStyles()
  const dispatch = useDispatch()
  const [modalForm, setModalForm] = React.useState(false)
  const usuario = useSelector((state) => state.auth.usuario)
  const posts = useSelector((state) => state.post.all)

  const callAll = useCallback(() => {
    dispatch(getMusicos())
    dispatch(getAllPosts())
  }, [dispatch])

  useEffect(() => {
    callAll()
  }, [callAll])

  function CriarPosts(id) {
    dispatch(createPosts(id))
  }

  const card = posts.map((item, k) => {
    const imagem = `http://localhost:3010${item.imagem}`
    if (item.usuarioNome === usuario.nome) {
      return (
        <Card key={k} className={classes.cardmusico}>
          <CardContent>
            <CardMedia
              className={classes.cardMedia}
              image={imagem}
            />
            <Typography className={classes.body}>
              {item.mensagem}
            </Typography>
          </CardContent>
        </Card>
      )
    }
    return (
      <div key={k} />
    )
  })

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Box flexGrow={1} className={classes.title}>
        <Typography variant="h6">Posts</Typography>
        <Button
          onClick={() => setModalForm(true)}
          variant="contained"
          color="primary"
          size="small"
          className={classes.button}
        >
          Novo
        </Button>
      </Box>
      <Grid className={classes.cardgrid} container spacing={4}>
        {card}
      </Grid>
      <DialogModal
        open={modalForm}
        close={() => setModalForm(false)}
        title="Posts"
      >
        <FormPost submit={CriarPosts} />
      </DialogModal>

    </div>

  )
}

export default PostCreate
