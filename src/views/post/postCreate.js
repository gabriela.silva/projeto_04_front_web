import React from 'react'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Grid from '@material-ui/core/Grid'
import Avatar from '@material-ui/core/Avatar'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import Box from '@material-ui/core/Box'
import { getAll as allMusicos } from '../../store/musicos/action'
import { getAll as allPosts } from '../../store/post/action'
import { getAll as allCategorias } from '../../store/categoria/action'
import PostCard from '../../components/post/postCard'

import { useDispatch, useSelector } from 'react-redux'

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2)
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6)
  },
  heroButtons: {
    marginTop: theme.spacing(4)
  },
  musicosgrid: {
    display: 'flex',
    flexWrap: 'wrap',
    boxSizing: 'border-box',
  },
  cardmusico: {
    margin: '7px',
    flexGrow: 1
  },
  title1: {
    color: '#402672'
  },
  title: {
    marginTop: '20px',
    color: '#402672'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  titlecard: {
    marginTop: '5px',
    fontSize: '16px',
    color: '#402672'
  },
  body: {
    fontSize: '14px',
    color: '#402672'
  }

}))

function Posts() {
  const classes = useStyles()
  const dispatch = useDispatch()
  const musicos = useSelector((state) => state.musicos.all)
  const posts = useSelector((state) => state.post.all)
  const [expanded, setExpanded] = React.useState(false)

  const handleExpandClick = () => {
    setExpanded(!expanded)
  }

  React.useEffect(() => {
    dispatch(allCategorias())
    dispatch(allMusicos())
    dispatch(allPosts())
  }, [dispatch])

  const card = musicos.map((item, k) => {
    if (item.status === 'Ativo') {
      return (
        <Card key={k} className={classes.cardmusico}>
          <CardContent className={classes.content}>
            <Avatar src={`http://localhost:3010${item.imagem}`} />
            <Typography className={classes.titlecard}>
              {item.nome.toUpperCase()}
            </Typography>
            <Typography className={classes.body}>
              {item.profile}
            </Typography>
            <Typography className={classes.body}>
              {item.cidade}
            </Typography>
            <Typography className={classes.body} gutterBottom>
              {item.email}
            </Typography>
          </CardContent>
        </Card>
      )
    }
    return null
  })

  function montarPost(data) {
    return data.map((card, i) => (
      <Grid className={classes.postscard} item key={i} xs={12} sm={6} md={3}>
        <PostCard data={card} />
      </Grid>
    ))
  }

  return (
    <main>
      <Box p={6}>
        <Container>
          <Box
            display="flex"
            borderBottom="thin solid #ccc"
            marginBottom={2}
            alignItems="center"
          >
            <Box flexGrow={1}>
              <Typography className={classes.title1} variant="h6">Posts</Typography>
            </Box>
          </Box>
          <Grid container spacing={4}>
            {montarPost(posts)}
          </Grid>
        </Container>
        <Container>
          <Box
            display="flex"
            borderBottom="thin solid #ccc"
            marginBottom={2}
            alignItems="center"
          >
            <Box className={classes.title} flexGrow={1}>
              <Typography variant="h6">Músicos</Typography>
            </Box>
          </Box>
          <Grid className={classes.musicosgrid}>
            {card}
          </Grid>
        </Container>
      </Box>
    </main>
  )
}

export default Posts
