import React from 'react'
import Profile from '../../components/ProfilePage/ProfilePageContent'
import PostPage from './postPage'

function PostRender() {
  return (
    <div>
      <Profile />
      <PostPage />
    </div>
  )
}

export default PostRender
