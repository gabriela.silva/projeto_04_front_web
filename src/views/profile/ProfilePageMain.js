import React from 'react'
import { useSelector } from 'react-redux'
import CssBaseline from '@material-ui/core/CssBaseline'
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import banner from '../../assets/images/banner2.png'
// import { Link as LinkRoute } from '@reach/router'
// import teste from '../../assets/logos/11.png'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },

  imgcontainer: {
    width: '100%',
  },
  banner: {
    opacity: '70%',
    height: '250px',
    width: '100%',
  },
  rootcard: {
    boxShadow: '0 16px 24px 2px rgb(0 0 0 / 20%), 0 6px 30px 5px rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%);',
    position: 'relative',
    margin: '-80px 40px 0px',
    borderRadius: '6px',
    zIndex: 3,
    textAlign: 'center',

  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  titlecard: {
    fontSize: 18,
    color: '#402672'
  },
  body: {
    color: '#402672'
  },
  pos: {
    marginBottom: 12,
    color: '#402672'
  },
  avatarcontainer: {
    display: 'inline-block',
    boxShadow:
      '5px 5px 15px -8px rgba(0, 0, 0, 0.24), 0 8px 10px -5px rgba(0, 0, 0, 0.2)',
    borderRadius: '50%',
    width: '150px',
    height: '150px',
    overflow: 'hidden'
  },
  imgCard: {
    width: '100%',
    height: '100%'
    // margin-left: -50px;
  }
}))

function ProfileMain(props) {
  const usuario = useSelector((state) => state.auth.usuario)
  // console.log(usuario)
  const classes = useStyles()
  const theme = useTheme()

  const avatar = `http://localhost:3010/static/musicos/${usuario.data.imagem.nome}`

  return (
    <div className={classes.root}>
      <CssBaseline />
      <div className={classes.toolbar} />
      {/* <div className={classes.drawerfill} /> */}
      <div className={classes.imgcontainer}>
        <img className={classes.banner} src={banner} />
      </div>
      <Card className={classes.rootcard} justify="center">
        <CardContent className={classes.cardcontent} xs={12} sm={12} md={6}>
          <div className={classes.avatarcontainer}>
            <img src={avatar} alt="..." className={classes.imgCard} />
          </div>
          <Typography className={classes.titlecard} gutterBottom>
            {usuario.nome}
          </Typography>
          <Typography className={classes.body} component="h2">
            {usuario.data.profile}
          </Typography>
          <Typography className={classes.pos} color="textSecondary">
            {usuario.data.generomusical}
          </Typography>
        </CardContent>
      </Card>
    </div>
  )
}

export default ProfileMain
