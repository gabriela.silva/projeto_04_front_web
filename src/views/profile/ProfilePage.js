import React from 'react'
// import { Router } from '@reach/router'
import Profile from '../../components/ProfilePage/ProfilePageContent'
import ProfileMain from '../../components/ProfilePage/ProfilePageMain'

function ProfilePage() {
  return (
    <>
      <Profile />
      <ProfileMain />
    </>
  )
}

export default ProfilePage
