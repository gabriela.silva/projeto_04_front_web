import React from 'react'
import { useSelector } from 'react-redux'
import CssBaseline from '@material-ui/core/CssBaseline'
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import { makeStyles } from '@material-ui/core/styles'

import teste from '../../assets/logos/11.png'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: '60px'
  },
  cardimagem: {
    boxShadow: '0 16px 24px 2px rgb(0 0 0 / 20%), 0 6px 30px 5px rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%);',
    borderRadius: '6px',
    marginBottom: '30px',
    textAlign: 'center',
    width: '400px',
    '@media (max-width: 450px)': {
      maxWidth: '200px'
    },
  },
  space: {
    flexGrow: 1
  },
  rootcard: {
    boxShadow: '0 16px 24px 2px rgb(0 0 0 / 20%), 0 6px 30px 5px rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%);',
    borderRadius: '6px',
    textAlign: 'center',
    width: '400px',
    '@media (max-width: 450px)': {
      maxWidth: '200px'
    },
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  titlecard: {
    fontSize: 18,
    color: '#402672'
  },
  pos: {
    marginBottom: 12,
  },
  avatarcontainer: {
    display: 'inline-block',
    boxShadow:
      '5px 5px 15px -8px rgba(0, 0, 0, 0.24), 0 8px 10px -5px rgba(0, 0, 0, 0.2)',
    borderRadius: '50%',
    width: '150px',
    height: '150px',
    overflow: 'hidden'
  },
  imgCard: {
    width: '100%',
    height: '100%'
    // margin-left: -50px;
  }
}))

function ClienteHome(props) {
  const usuario = useSelector((state) => state.auth.usuario)
  // console.log(usuario)
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <CssBaseline />
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Card className={classes.cardimagem}>
          <CardContent className={classes.cardcontent} xs={12} sm={12} md={6}>
            <div className={classes.avatarcontainer}>
              <img src={teste} alt="..." className={classes.imgCard} />
            </div>
            <Typography className={classes.titlecard}>
              Tuned_In
            </Typography>
          </CardContent>
        </Card>
        <div className={classes.space} />
        <Card className={classes.rootcard} justify="center">
          <CardContent className={classes.cardcontent} xs={12} sm={12} md={6}>
            <Typography className={classes.titlecard} gutterBottom>
              {usuario.nome}
            </Typography>
            <Typography component="h2">
              {usuario.data.descricao}
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
              {usuario.data.endereco}, {usuario.data.cidade}, {usuario.data.uf}
            </Typography>
            <Typography variant="body2" component="p">
              {usuario.data.telefone}
            </Typography>
          </CardContent>
        </Card>
      </main>
    </div>
  )
}

export default ClienteHome
