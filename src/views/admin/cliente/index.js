import React, { useEffect, useCallback } from 'react'
import { Grid, CssBaseline, Tooltip, IconButton } from '@material-ui/core'
import { BsToggleOff, BsToggleOn } from 'react-icons/bs'
import { useDispatch, useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import Title from '../../../components/title/index'
import DataList from '../../../components/datagrid'

import {
  getAll as getClientes,
  setStatusCliente
} from '../../../store/cliente/action'

const useStyles = makeStyles((theme) => ({
  cliente: {
    marginTop: '30px'
  },
  icon: {
    color: '#402672'
  }
}))

function Cliente() {
  const dispatch = useDispatch()
  const classes = useStyles()
  const clientes = useSelector((state) => state.cliente.all)
  const loading = useSelector((state) => state.cliente.loading)

  const callMusicos = useCallback(() => {
    dispatch(getClientes())
  }, [dispatch])

  useEffect(() => {
    callMusicos()
  }, [callMusicos])

  const toggleActive = (id, status) => {
    dispatch(setStatusCliente(id, status))
  }
  const actionModal = ({ id, row }) => {
    const status = row.status === 'Ativo'
    return (
      <>
        <Tooltip title={status ? 'Desativar' : 'Ativar'}>
          <IconButton onClick={() => toggleActive(id, status)} color="primary">
            <>{!status ? <BsToggleOff className={classes.icon} /> : <BsToggleOn className={classes.icon} />}</>
          </IconButton>
        </Tooltip>
      </>
    )
  }

  const columns = [
    { field: 'nome', headerName: 'Nome', flex: 1, disableColumnMenu: true },
    {
      field: 'descricao',
      headerName: 'Descrição',
      flex: 1,
      disableColumnMenu: true
    },
    {
      field: 'actions',
      headerName: 'Ações',
      renderCell: actionModal,
      width: 140,
      disableColumnMenu: true
    }
  ]

  const actions = () => null

  return (
    <div className={classes.cliente}>
      <Title title="Clientes" actions={actions} />
      <Grid container spacing={2}>
        <CssBaseline />
        <Grid item xs={12} md={12} xl={12}>
          <DataList data={clientes} columns={columns} loading={loading} />
        </Grid>
      </Grid>
    </div>
  )
}

export default Cliente
