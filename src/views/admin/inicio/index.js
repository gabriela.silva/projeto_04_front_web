import React from 'react'
import { Pie } from 'react-chartjs-2'
import { useDispatch, useSelector } from 'react-redux'
import { Grid } from '@material-ui/core'
import Title from '../../../components/title/index'
import { makeStyles } from '@material-ui/core/styles'

import { getAll as getPosts } from '../../../store/post/action'
import { getAll as getCategorias } from '../../../store/categoria/action'
import { getAll as getMusicos } from '../../../store/musicos/action'
import { getAll as getClientes } from '../../../store/cliente/action'

const useStyles = makeStyles((theme) => ({
  inicio: {
    marginTop: '30px'
  }
}))

function Inicio() {
  const classes = useStyles()
  const dispatch = useDispatch()
  const musicos = useSelector((state) => state.musicos.all)
  const categorias = useSelector((state) => state.categoria.all)
  const posts = useSelector((state) => state.post.all)
  const clientes = useSelector((state) => state.cliente.all)

  React.useEffect(() => {
    dispatch(getMusicos())
    dispatch(getCategorias())
    dispatch(getPosts())
    dispatch(getClientes())
  }, [dispatch])

  const actions = () => null

  const data = {
    labels: ['Muscios', 'Categorias', 'Posts', 'Clientes'],
    datasets: [
      {
        label: '# of Votes',
        data: [musicos.length, categorias.length, posts.length, clientes.length],
        backgroundColor: [
          'rgba(243, 111, 169, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(64, 38, 114, 0.2)',
        ],
        borderColor: [
          'rgba(243, 111, 169, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(64, 38, 114, 1)',
        ],
        borderWidth: 1
      }
    ]
  }

  return (
    <div className={classes.inicio}>
      <Title title="Chart" actions={actions} />
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <Grid container>
            <Grid item>
              <Pie data={data} />
            </Grid>
          </Grid>
        </Grid>
        {/* <Grid item xs={6}>
        <Paper>
          <Grid container>
            <Grid item>

            </Grid>
          </Grid>
        </Paper>
      </Grid> */}
      </Grid>
    </div>
  )
}

export default Inicio
