import React, { useEffect, useCallback } from 'react'
import { Grid, CssBaseline, IconButton, Tooltip } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import Title from '../../../components/title'
import DataList from '../../../components/datagrid'
import { BsToggleOff, BsToggleOn } from 'react-icons/bs'
import { More as MoreIcon } from '@material-ui/icons'

import {
  getAll as getMusicos,
  setStatusMusicos,
  obterPost
} from '../../../store/musicos/action'

import ListaPosts from '../../../components/admin/musicos/posts'

const useStyles = makeStyles((theme) => ({
  musicos: {
    marginTop: '30px'
  },
  icon: {
    color: '#402672'
  }
}))

function Musicos () {
  const classes = useStyles()
  const dispatch = useDispatch()
  const [modalPost, setModalPost] = React.useState(false)

  const { tipoUsuario } = useSelector((state) => state.auth.usuario)
  const musicos = useSelector((state) => state.musicos.all)
  const loading = useSelector((state) => state.musicos.loading)

  const callMusicos = useCallback(() => {
    dispatch(getMusicos())
  }, [dispatch])

  useEffect(() => {
    callMusicos()
  }, [callMusicos])

  const toggleActive = (id, status) => {
    dispatch(setStatusMusicos(id, status))
  }

  function openPosts(id) {
    dispatch(obterPost(id)).then(() => setModalPost(true))
  }

  const actionModal = ({ id, row }) => {
    const status = row.status === 'Ativo'
    return (
      <>
        <Tooltip title="Listar de Posts">
          <IconButton onClick={() => openPosts(id)} color="primary">
            <MoreIcon className={classes.icon} />
          </IconButton>
        </Tooltip>
        <div>
          {tipoUsuario === 1
            ? (
              <Tooltip title={status ? 'Desativar' : 'Ativar'}>
                <IconButton onClick={() => toggleActive(id, status)} color="primary">
                  <>{!status ? <BsToggleOff className={classes.icon} /> : <BsToggleOn className={classes.icon} />}</>
                </IconButton>
              </Tooltip>
              )
            : (
                ''
              )}
        </div>

      </>
    )
  }

  const columns = [
    {
      field: 'nome',
      headerName: 'Nome',
      flex: 1,
      disableColumnMenu: true
    },
    {
      field: 'profile',
      headerName: 'Profile',
      flex: 1,
      disableColumnMenu: true
    },
    {
      field: 'actions',
      headerName: 'Ações',
      renderCell: actionModal,
      width: 140,
      disableColumnMenu: true
    }
  ]

  const actions = () => null

  return (
    <div className={classes.musicos}>
      <Title
        title="Musicos"
        subTitle="Pagina de Categorias"
        actions={actions}
      />
      <Grid container spacing={2}>
        <CssBaseline />
        <Grid item xs={12} md={12} xl={12}>
          <DataList data={musicos} columns={columns} loading={loading} />
        </Grid>
      </Grid>
      <ListaPosts open={modalPost} close={() => setModalPost(false)} />
    </div>
  )
}

export default Musicos
