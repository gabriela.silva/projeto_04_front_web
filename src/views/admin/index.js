import { Router } from '@reach/router'
import MainPainel from '../../components/layout/mainPainel'
import Inicio from './inicio/index'
import Search from '../post/postCreate'
import Musicos from './musicos'
import Categoria from './categoria/index'
import Cliente from './cliente/index'
import ClienteHome from '../cliente/ClienteHome'
import MusicoProfile from '../profile/ProfilePageMain'
import Posts from '../post/postPage'

import {
  Dashboard as DashboardIcon,
  Apps as MdAppsIcon,
  People as PeopleIcon,
  MusicNote as MusicNoteIcon,
  Subject as SubjectIcon,
  Search as SearchIcon,
  AccountCircle as AccountCircleIcon,
} from '@material-ui/icons'

import { useSelector } from 'react-redux'

export const Menu = [
  {
    title: 'Home',
    icons: <DashboardIcon />,
    route: '/admin',
    visibleMenu: true,
    enabled: true,
    component: Inicio,
    authorization: [1]
  },
  {
    title: 'Home',
    icons: <DashboardIcon />,
    route: '/cliente',
    visibleMenu: true,
    enabled: true,
    component: ClienteHome,
    authorization: [3]
  },
  {
    title: 'Profile',
    icons: <AccountCircleIcon />,
    route: '/musicos',
    visibleMenu: true,
    enabled: true,
    component: MusicoProfile,
    authorization: [2]
  },
  {
    title: 'Categoria',
    icons: <MdAppsIcon />,
    route: '/categoria',
    visibleMenu: true,
    enabled: true,
    component: Categoria,
    authorization: [1]
  },
  {
    title: 'Músicos',
    icons: <MusicNoteIcon />,
    route: '/musicos',
    visibleMenu: true,
    enabled: true,
    component: Musicos,
    authorization: [1, 3]
  },
  {
    title: 'Posts',
    icons: <SubjectIcon />,
    route: '/posts',
    visibleMenu: true,
    enabled: true,
    component: Posts,
    authorization: [2]
  },
  {
    title: 'Cliente',
    icons: <PeopleIcon />,
    route: '/cliente',
    visibleMenu: true,
    enabled: true,
    component: Cliente,
    authorization: [1]
  },
  {
    title: 'Search',
    icons: <SearchIcon />,
    route: '/search',
    visibleMenu: true,
    enabled: true,
    component: Search,
    authorization: [2, 3]
  }
]

const Admin = (props) => {
  const tipoUsuario = useSelector((state) => state.auth.usuario.tipoUsuario)
  const rotasAutorizadas = Menu.filter((route) =>
    route.authorization.includes(tipoUsuario)
  )

  const NotFound = () => <h2>Não autorizado</h2>

  return (
    <Router>
      <MainPainel path="/">
        {rotasAutorizadas.map(({ component: Component, route }, i) => (
          <Component key={i} path={route} />
        ))}
        <NotFound default />
      </MainPainel>
    </Router>
  )
}

export default Admin
