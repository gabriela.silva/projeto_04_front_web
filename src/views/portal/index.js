import styled from 'styled-components'
import Header from '../../components/layout/header'
import Footer from '../../components/layout/footer'
import Main from '../../components/layout/main'

const Portal = (props) => {
  return (
    <>
      <Header />
      <Main />
      <SFooter />
    </>
  )
}

export default Portal

const SFooter = styled(Footer)`
  position: absolute;
`
