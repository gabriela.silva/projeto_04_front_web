import React, { useEffect, useState } from 'react'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import { Link } from '@reach/router'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import UfCidade from '../../utils/estados-cidades.json'
import styled from 'styled-components'
import { makeStyles } from '@material-ui/core/styles'

import { create as createMusicos } from '../../store/musicos/action'
import { useDispatch, useSelector } from 'react-redux'
import { SignBox, FormStyle, Submit, LoadingSubmit } from './styles'
import { FormControl, Select, Avatar } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  button: {
    backgroundColor: '#402672',
    color: '#FFF',
    marginTop: '10px'
  },
  link: {
    textDecoration: 'none !important',
    color: '#402672'
  }
}))

const MusicoNovo = () => {
  const [preview, setPreview] = useState('')
  const dispatch = useDispatch()
  const [uf, setUf] = useState([])
  const [cidades, setCidade] = useState([])
  const [form, setForm] = useState({})
  const loading = useSelector((state) => state.auth.loading)

  const handleChange = (props) => {
    const { value, name } = props.target
    setForm({
      ...form,
      [name]: value
    })
  }

  const submitForm = () => {
    dispatch(createMusicos(form))
  }

  useEffect(() => {
    const estados = UfCidade.estados.map(({ nome, sigla }) => ({ nome, sigla }))
    setUf(estados)
  }, [])

  useEffect(() => {
    const result = UfCidade.estados.find((item) => item.sigla === form.uf)
    if (result) {
      setCidade(result.cidades)
    }
  }, [form.uf])

  const removeImage = () => {
    delete form.imagem
    setForm(form)
    setPreview('')
  }
  const previewImg = (props) => {
    const imagem = props.target.files[0]
    const url = URL.createObjectURL(imagem)
    setPreview(url)
    setForm({
      ...form,
      imagem
    })
  }
  const classes = useStyles()
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <SignBox>
        <Typography className={classes.title} component="h1" variant="h5">
          Cadastro de Músico
        </Typography>
        <FormStyle noValidate>
          {preview.length > 0
            ? (
              <div className={classes.root}>
                <Avatar className={classes.large}>
                  <Image src={preview} />
                </Avatar>
                <Button onClick={removeImage} className={classes.button} component="label">
                  Remove
                </Button>
              </div>
              )
            : (
              <Button
                className={classes.button}
                variant="contained"
                size="small"
                component="label"
              >
                Upload Foto
                <input
                  accept="image/*"
                  type="file"
                  name="imagem"
                  hidden
                  onChange={previewImg}
                />
              </Button>
              )}
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="nome"
            label="Informe seu nome"
            name="nome"
            autoComplete="nome"
            autoFocus
            value={form.nome || ''}
            onChange={handleChange}
            disabled={loading}
            size="small"
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="profile"
            label="Quem é você?"
            name="profile"
            autoComplete="profile"
            autoFocus
            value={form.profile || ''}
            onChange={handleChange}
            disabled={loading}
            size="small"
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="generomusical"
            label="Que tipo(s) de musica você toca?"
            name="generomusical"
            autoComplete="generomusical"
            autoFocus
            value={form.generomusical || ''}
            onChange={handleChange}
            disabled={loading}
            size="small"
          />
          <Grid container spacing={2}>
            <Grid item sm={4} md={4} xl={4}>
              <FormControl
                variant="outlined"
                fullWidth
                size="small"
                margin="normal"
              >
                <Select
                  native
                  value={form.uf || ''}
                  onChange={handleChange}
                  inputProps={{
                    name: 'uf',
                    id: 'outlined-native-simple'
                  }}
                >
                  <option value="">Uf</option>
                  {uf?.map(({ nome, sigla }, i) => (
                    <option key={i} value={sigla}>
                      {sigla}
                    </option>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item sm={8} md={8} xl={8}>
              <FormControl
                variant="outlined"
                size="small"
                fullWidth
                margin="normal"
                disabled={!form.uf}
              >
                <Select
                  fullWidth
                  native
                  value={form.cidade || ''}
                  onChange={handleChange}
                  inputProps={{
                    name: 'cidade',
                    id: 'outlined-native-simple'
                  }}
                >
                  <option value="">Cidade</option>

                  {cidades?.map((cidade, i) => (
                    <option key={i} value={cidade}>
                      {cidade}
                    </option>
                  ))}
                </Select>
              </FormControl>
            </Grid>
          </Grid>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Informe seu endereço de e-mail"
            name="email"
            autoComplete="email"
            autoFocus
            value={form.email || ''}
            onChange={handleChange}
            disabled={loading}
            size="small"
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="senha"
            label="Informe sua senha"
            type="password"
            id="senha"
            autoComplete="current-password"
            value={form.senha || ''}
            onChange={handleChange}
            disabled={loading}
            size="small"
          />
          <Submit>
            <Button
              size="large"
              type="submit"
              fullWidth
              variant="contained"
              className={classes.button}
              onClick={submitForm}
              disabled={loading}
            >
              {loading ? <LoadingSubmit size={24} /> : 'Cadastrar'}
            </Button>
          </Submit>
          <Grid container>
            <Grid item>
              Já possui cadastro?
              <Link to="/signin" variant="body2" className={classes.link}>
                &ensp;Faça o Login
              </Link>
            </Grid>
          </Grid>
        </FormStyle>
      </SignBox>
    </Container>
  )
}

export default MusicoNovo

const Image = styled.img`
  border: thin solid #eee;
  border-radius: 3px;
  overflow: hidden;
`
const AvatarBox = styled.div`

`
