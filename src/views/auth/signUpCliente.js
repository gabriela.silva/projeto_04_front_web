import React, { useEffect, useState } from 'react'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import { Link } from '@reach/router'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import UfCidade from '../../utils/estados-cidades.json'
import styled from 'styled-components'
import { create as createCliente } from '../../store/cliente/action'
import { useDispatch, useSelector } from 'react-redux'
import { SignBox, FormStyle, Submit, LoadingSubmit } from './styles'
import { FormControl, Select } from '@material-ui/core'
import InputMask from 'react-input-mask'

const ClienteNovo = () => {
  const dispatch = useDispatch()
  const [uf, setUf] = useState([])
  const [cidades, setCidade] = useState([])
  const [form, setForm] = useState({})
  const loading = useSelector((state) => state.auth.loading)

  const handleChange = (props) => {
    const { value, name } = props.target
    setForm({
      ...form,
      [name]: value
    })
  }

  const submitForm = () => {
    dispatch(createCliente(form))
  }

  useEffect(() => {
    const estados = UfCidade.estados.map(({ nome, sigla }) => ({ nome, sigla }))
    setUf(estados)
  }, [])

  useEffect(() => {
    const result = UfCidade.estados.find((item) => item.sigla === form.uf)
    if (result) {
      setCidade(result.cidades)
    }
  }, [form.uf])

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <SignBox>
        <Typography component="h1" variant="h5">
          Cadastro de Cliente
        </Typography>
        <FormStyle noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="nome"
            label="Informe seu nome"
            name="nome"
            autoComplete="nome"
            autoFocus
            value={form.nome || ''}
            onChange={handleChange}
            disabled={loading}
            size="small"
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="descricao"
            label="Quem é você?"
            name="descricao"
            autoComplete="descricao"
            autoFocus
            value={form.descricao || ''}
            onChange={handleChange}
            disabled={loading}
            size="small"
          />
          <InputMask
            mask="(99)99999-9999"
            value={form.telefone || ''}
            disabled={false}
            maskChar=" "
            onChange={handleChange}
          >
            {() => (
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="telefone"
                label="Informe seu telefone"
                name="telefone"
                autoComplete="telefone"
                autoFocus
                value={form.telefone || ''}
                disabled={loading}
                size="small"
              />
            )}
          </InputMask>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="endereco"
            label="Informe seu endereco"
            name="endereco"
            autoComplete="endereco"
            autoFocus
            value={form.endereco || ''}
            onChange={handleChange}
            disabled={loading}
            size="small"
          />
          <Grid container spacing={2}>
            <Grid item sm={4} md={4} xl={4}>
              <FormControl
                variant="outlined"
                fullWidth
                size="small"
                margin="normal"
              >
                <Select
                  native
                  value={form.uf || ''}
                  onChange={handleChange}
                  inputProps={{
                    name: 'uf',
                    id: 'outlined-native-simple'
                  }}
                >
                  <option value="">Uf</option>
                  {uf?.map(({ nome, sigla }, i) => (
                    <option key={i} value={sigla}>
                      {sigla}
                    </option>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item sm={8} md={8} xl={8}>
              <FormControl
                variant="outlined"
                size="small"
                fullWidth
                margin="normal"
                disabled={!form.uf}
              >
                <Select
                  fullWidth
                  native
                  value={form.cidade || ''}
                  onChange={handleChange}
                  inputProps={{
                    name: 'cidade',
                    id: 'outlined-native-simple'
                  }}
                >
                  <option value="">Cidade</option>

                  {cidades?.map((cidade, i) => (
                    <option key={i} value={cidade}>
                      {cidade}
                    </option>
                  ))}
                </Select>
              </FormControl>
            </Grid>
          </Grid>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Informe seu endereço de e-mail"
            name="email"
            autoComplete="email"
            autoFocus
            value={form.email || ''}
            onChange={handleChange}
            disabled={loading}
            size="small"
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="senha"
            label="Informe sua senha"
            type="password"
            id="senha"
            autoComplete="current-password"
            value={form.senha || ''}
            onChange={handleChange}
            disabled={loading}
            size="small"
          />
          <Submit>
            <Sbutton
              size="large"
              className="buttonSubmit"
              type="submit"
              fullWidth
              variant="contained"
              onClick={submitForm}
              disabled={loading}
            >
              {loading ? <LoadingSubmit size={24} /> : 'Cadastrar'}
            </Sbutton>
          </Submit>
          <Grid container>
            <Grid item>
              Já possui cadastro?
              <SLink to="/signin" variant="body2">
                &ensp;Faça o Login
              </SLink>
            </Grid>
          </Grid>
        </FormStyle>
      </SignBox>
    </Container>
  )
}

export default ClienteNovo

const Sbutton = styled(Button)`
  background-color: #402672;
  color: #fff;
`
const SLink = styled(Link)`
  text-decoration: none !important;
  color: #402672;
`
