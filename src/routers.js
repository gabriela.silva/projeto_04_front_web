import React from 'react'
import { Router, Redirect } from '@reach/router'
import AdminView from './views/admin'
import MusicoPosts from './views/post/index'
import PortalView from './views/portal'
import SignIn from './views/auth/signin'
import { isAuthenticated } from './config/storage'
import MusicoNovo from './views/auth/signUpMusico'
import ClienteNovo from './views/auth/signUpCliente'

const PrivateRoute = ({ component: Component, ...rest }) => {
  if (!isAuthenticated()) {
    return <Redirect to="/signin" noThrow />
  }
  return <Component {...rest} />
}

const Routers = () => (
  <>
    <Router>
      <SignIn path="signin" />
      <MusicoNovo path="signUpMusico" />
      <ClienteNovo path="signUpCliente" />
      {/* Portal */}
      <PortalView exact path="/*" />
      {/* Admin */}
      <PrivateRoute component={AdminView} path="/admin/*" />
      {/* Musicos */}
      <PrivateRoute component={AdminView} exact path="/musicos/*" />
      <PrivateRoute component={MusicoPosts} exact path="/posts" />
      {/* Cliente */}
      <PrivateRoute component={AdminView} exact path="/cliente/*" />
    </Router>
  </>
)

export default Routers
