import {
  create as createMusicos,
  getAll as getAllMusicos,
  getbyId as getMusicosById,
  update as updateMusicos,
  remove as removeMusicos,
  obterListadePosts,
  ativarMusicos,
  inativaMusicos,
  getbyId
} from '../../services/musicos.service'
import TYPES from '../types'
import { toastr } from 'react-redux-toastr'
import { navigate } from '@reach/router'

export const create = (data) => {
  return async (dispatch) => {
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
    }
    try {
      const formData = new FormData()
      Object.keys(data).map((k) => formData.append(k, data[k]))
      const result = await createMusicos(formData, config)
      dispatch({ type: TYPES.MUSICOS_CREATE, data: result.data.data })
      toastr.success('Cadastro', 'Músico cadastrado com sucesso, aguarde e-mail de aprovação.')
      navigate('/')
    } catch (error) {
      toastr.error('Cadastro', 'Erro', error)
    }
  }
}

export const edit = (id) => {
  return async (dispatch) => {
    dispatch({
      type: TYPES.MUSICOS_UPLOAD,
      upload: 0
    })
    try {
      const result = await getMusicosById(id)
      dispatch({ type: TYPES.MUSICOS_EDIT, data: result.data })
    } catch (error) {
      toastr.error('aconteceu um erro', error)
    }
  }
}
export const getAll = () => {
  return async (dispatch) => {
    try {
      dispatch({ type: TYPES.MUSICOS_LOADING, status: true })
      const result = await getAllMusicos()
      dispatch({ type: TYPES.MUSICOS_ALL, data: result.data?.data })
    } catch (error) {
      toastr.error('aconteceu um erro', error)
    }
  }
}
export const getLoggedById = (musicosid) => {
  return async (dispatch) => {
    try {
      dispatch({ type: TYPES.MUSICOS_LOADING, status: true })
      const result = await getbyId(musicosid)
      dispatch({ type: TYPES.MUSICOS_BYID, data: result })
    } catch (error) {
    }
  }
}
export const update = ({ id, ...data }) => {
  return (dispatch) => {
    dispatch({ type: TYPES.MUSICOS_LOADING, status: true })
    dispatch({
      type: TYPES.MUSICOS_UPLOAD,
      upload: 0
    })
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      onUploadProgress: function (progressEvent) {
        const percentCompleted = Math.round(
          (progressEvent.loaded * 100) / progressEvent.total
        )
        // console.log('percentCompleted', percentCompleted)

        dispatch({
          type: TYPES.MUSICOS_UPLOAD,
          upload: percentCompleted
        })
      }
    }

    const formData = new FormData()
    Object.keys(data).map((k) => formData.append(k, data[k]))
    updateMusicos(id, formData, config)
      .then((result) => {
        dispatch(edit(id))
        dispatch(getAll())
        toastr.success('Músico', 'Músico atualizada com sucesso')
        dispatch({ type: TYPES.MUSICOS_EDIT })
      })
      .catch((error) => {
        dispatch({ type: TYPES.SIGN_ERROR, data: error })
        toastr.error('Músico', error.toString())
      })
  }
}
export const remove = (id) => {
  return async (dispatch) => {
    try {
      const result = await removeMusicos(id)
      dispatch({ type: TYPES.MUSICOS_EDIT, data: result.data })
      toastr.success('Músico', 'Removido com sucesso')
      dispatch(getAll())
    } catch (error) {
      toastr.error('aconteceu um erro', error)
      toastr.error('Músico', error.toString())
    }
  }
}

export const setStatusMusicos = (id, ativo) => {
  return async (dispatch, getState) => {
    let result
    try {
      if (ativo) {
        result = await inativaMusicos(id)
        toastr.success(
          `Músico ${result.data.data.nome}`,
          'Desativado com sucesso'
        )
      } else {
        result = await ativarMusicos(id)
        toastr.success(
          `Músico ${result.data.data.nome}`,
          'Ativado com sucesso'
        )
      }
      const all = getState().musicos.all
      const index = all.findIndex((item) => item.id === id)
      all[index].status = result.data.data.status

      dispatch({ type: TYPES.MUSICOS_ALL, data: [...all] })
    } catch (err) {
      // console.log('###', err)
    }
  }
}

export const obterPost = (usuarioid) => {
  return async (dispatch) => {
    try {
      const result = await obterListadePosts(usuarioid)
      console.log('%%%%%', result)
      dispatch({ type: TYPES.MUSICOS_POST, data: result.data.result })
    } catch (error) {
      toastr.error('Músico', 'Erro ao carregar posts')
    }
  }
}

// export const likePost = ({ nome, musicosId }) => {
//   return async (dispatch) => {
//     try {
//       const result = await likeMusicoService(musicosId)
//       toastr.success('Curtida', `o Post ${nome} curtido com sucesso.`)
//     } catch (error) {
//       toastr.error('Curtida', 'Erro ao fazer curtida')
//     }
//   }
// }
