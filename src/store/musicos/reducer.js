import TYPES from '../types'

const INITIAL_STATE = {
  loading: false,
  all: [],
  id: {},
  upload: {},
  selected: {},
  post: []
}

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.MUSICOS_LOADING:
      state.error = []
      state.loading = action.status
      return state
    case TYPES.MUSICOS_ALL:
      state.all = action.data
      state.loading = false
      return state
    case TYPES.MUSICOS_EDIT:
      state.selected = action.data
      state.loading = false
      return state
    case TYPES.MUSICOS_UPLOAD:
      state.upload = action.upload
      return state
    case TYPES.MUSICOS_POST:
      state.post = action.data
      return state
    case TYPES.MUSICOS_CREATE:
      state.loading = false
      return state
    case TYPES.MUSICOS_BYID:
      state.id = action.data
      return state
    default:
      return state
  }
}

export default reducer
