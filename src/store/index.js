// import as libs
import { applyMiddleware, combineReducers, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
// import multi from 'redux-multi'

// importação dos reducers
import { reducer as toastrReducer } from 'react-redux-toastr'
import SignReducer from './auth/auth.reducer'
import CategoriaReducer from './categoria/reducer'
import PostReducer from './post/reducer'
import MusicosReducer from './musicos/reducer'
import ClienteReducer from './cliente/reducer'

const reducers = combineReducers({
  auth: SignReducer,
  toastr: toastrReducer,
  categoria: CategoriaReducer,
  musicos: MusicosReducer,
  post: PostReducer,
  cliente: ClienteReducer
})

// middlewares de redux
const middlewares = [thunk]

// compose junta os middlewares e ferramentas de debug

const compose = composeWithDevTools(applyMiddleware(...middlewares))

// criar a store do redux

const store = createStore(reducers, compose)

export default store
