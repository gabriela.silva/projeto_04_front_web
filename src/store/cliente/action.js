import {
  create as ClienteCreate,
  getAll as getAllCliente,
  inativaCliente,
  ativarCliente
} from '../../services/cliente.service'
import TYPES from '../types'
import { toastr } from 'react-redux-toastr'
import { navigate } from '@reach/router'

export const create = (data) => {
  return async (dispatch) => {
    try {
      const result = await ClienteCreate(data)
      // console.log(result)
      toastr.success('Cliente', 'Cliente cadastrada com sucesso, aguarde e-mail de aprovação.')
      navigate('/')
    } catch (error) {
      toastr.error('Cliente', 'deu ruim')
    }
    // console.log('disparar...', data)
  }
}

export const getAll = () => {
  return async (dispatch) => {
    try {
      dispatch({ type: TYPES.CLIENTE_LOADING, status: true })
      const result = await getAllCliente()
      dispatch({ type: TYPES.CLIENTE_ALL, data: result.data.data })
    } catch (error) {
      toastr.error('aconteceu um erro', error)
    }
  }
}

export const setStatusCliente = (id, ativo) => {
  // console.log('Cliente mudar status', ativo)
  return async (dispatch, getState) => {
    let result
    try {
      if (ativo) {
        result = await inativaCliente(id)
        toastr.success(
          `Cliente ${result.data.data.nome}`,
          'Desativado com sucesso'
        )
      } else {
        result = await ativarCliente(id)
        toastr.success(
          `Cliente ${result.data.data.nome}`,
          'Ativado com sucesso'
        )
      }
      const all = getState().cliente.all
      const index = all.findIndex((item) => item.id === id)
      all[index].status = result.data.data.status

      dispatch({ type: TYPES.CLIENTE_ALL, data: [...all] })
    } catch (err) {
      // console.log('###', err)
    }
  }
}
