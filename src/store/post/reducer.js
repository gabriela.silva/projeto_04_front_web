import TYPES from '../types'

const INITIAL_STATE = {
  loading: false,
  all: [],
  upload: {},
  selected: {}
}

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.POST_LOADING:
      state.error = []
      state.loading = action.status
      return state
    case TYPES.POST_ALL:
      state.all = action.data
      state.loading = false
      return state
    case TYPES.POST_EDIT:
      state.selected = action.data
      state.loading = false
      return state
    case TYPES.POST_UPLOAD:
      state.upload = action.upload
      return state
    case TYPES.POST_CREATE:
      state.loading = false
      return state
    case TYPES.POST_BYMUSICO:
      state.getByMusico = action.data
      return state
    default:
      return state
  }
}

export default reducer
