import {
  create as createPosts,
  getAll as getAllPosts,
} from '../../services/post.service'
import TYPES from '../../store/types'
import { toastr } from 'react-redux-toastr'

export const create = (data) => {
  return async (dispatch, getState) => {
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      onUploadProgress: function (progressEvent) {
        const percent = Math.round(
          (progressEvent.loaded * 100) / progressEvent.total
        )
        dispatch({
          type: TYPES.POST_UPLOAD,
          upload: {
            finish: percent === 100,
            percent: percent
          }
        })
      }
    }
    try {
      const formData = new FormData()
      Object.keys(data).map((k) => formData.append(k, data[k]))
      const usuarioid = getState().auth.usuario.id
      const result = await createPosts(usuarioid, formData, config)
      dispatch({ type: TYPES.POST_CREATE, data: result.data })
      toastr.success('Post', 'Post publicado com sucesso')
      dispatch(getAll())
    } catch (error) {
      toastr.error('Post', 'deu ruim')
    }
  }
}
export const getAll = () => {
  return async (dispatch) => {
    try {
      dispatch({ type: TYPES.POST_LOADING, status: true })
      const result = await getAllPosts()
      dispatch({ type: TYPES.POST_ALL, data: result.data.data })
    } catch (error) {
      toastr.error('aconteceu um erro', error)
    }
  }
}
