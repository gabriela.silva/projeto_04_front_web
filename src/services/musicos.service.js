import http from '../config/http'

const baseUrl = '/musicos'

export const getAll = () => http.get(baseUrl)

export const getbyId = (musicosid) => http.get(`${baseUrl}/${musicosid}`)

export const update = (id, data, config = {}) =>
  http.put(`${baseUrl}/${id}`, data, config)

export const remove = (id) => http.delete(`${baseUrl}/${id}`)

export const create = (data, config = {}) => http.post(baseUrl, data, config)

export const ativarMusicos = (id) => http.put(`${baseUrl}/${id}/ativa`)

export const inativaMusicos = (id) => http.put(`${baseUrl}/${id}/inativa`)

export const obterListadePosts = (usuarioid) => http.get(`${baseUrl}/${usuarioid}/post`)

export const likeMusicoService = (idmusicos) =>
  http.post(`${baseUrl}/${idmusicos}/curtidas`)
