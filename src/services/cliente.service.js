import http from '../config/http'

const baseUrl = '/cliente'

export const getAll = () => http.get(baseUrl)

export const create = (data) => http.post(baseUrl, data)

export const ativarCliente = (id) => http.put(`${baseUrl}/${id}/ativa`)

export const inativaCliente = (id) => http.put(`${baseUrl}/${id}/inativa`)
