import http from '../config/http'

const baseUrl = '/posts'

export const getAll = () => http.get(baseUrl)

// export const remove = (id) => http.delete(`${baseUrl}/${id}`)

export const create = (usuarioid, data, config = {}) =>
  http.post(`musicos/${usuarioid}/posts`, data, config)
