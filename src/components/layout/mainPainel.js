import React from 'react'
import clsx from 'clsx'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import CssBaseline from '@material-ui/core/CssBaseline'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Hidden from '@material-ui/core/Hidden'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import Box from '@material-ui/core/Box'
import Tooltip from '@material-ui/core/Tooltip'
import MainListItems from './painelItem'
import Logo from '../../assets/logos/10.2.png'
import styled from 'styled-components'
import { useDispatch, useSelector } from 'react-redux'
import { logoutAction } from '../../store/auth/auth.action'
import { Button } from '@material-ui/core'

const drawerWidth = 210

export default function MiniDrawer(props) {
  const { nome, tipoUsuario } = useSelector((state) => state.auth.usuario)

  const dispatch = useDispatch()

  const userDefinition = {
    1: {
      titulo: 'Administrador',
      cor: 'rgba(64, 38, 114, 1)'
    },
    2: {
      titulo: 'Músicos',
      cor: 'rgba(180, 144, 199)'
    },
    3: {
      titulo: 'Cliente',
      cor: '#F283AF'
    }
  }

  const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      padding: 0,
      width: '100%',
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      backgroundColor: tipoUsuario ? userDefinition[tipoUsuario].cor : '#666',
    },
    appBarShift: {
      [theme.breakpoints.up('sm')]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        flexFlow: 'row',
        justifyContent: 'flex-end',
      },
    },
    menuButton: {
      marginRight: theme.spacing(2),
      [theme.breakpoints.up('sm')]: {
        display: 'none',
      },
    },
    hide: {
      display: 'none',
    },
    drawer: {
      [theme.breakpoints.up('sm')]: {
        width: drawerWidth,
        flexShrink: 0,
      },
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(9) + 1,
      },
    },
    drawerPaper: {
      width: drawerWidth,
    },
    toolbar: {
      ...theme.mixins.toolbar,
    },
    toolbarIcon: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: '0'
    },
    user: {
      padding: theme.spacing(1),
      display: 'flex',
      alignItems: 'center'
    },
    userIcon: {
      margin: theme.spacing(1),
      color: 'white'
    },
    title: {
      flexGrow: 1
    },
    rootmain: {
      display: 'flex',
      width: '100%',
      marginTop: '40px',
    },
    containermain: {
      width: '100%',
    }

  }))
  const classes = useStyles()
  const theme = useTheme()
  const [mobileOpen, setMobileOpen] = React.useState(false)

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen)
  }

  function handleLogout() {
    dispatch(logoutAction())
  }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerToggle}
            edge="start"
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            component="h1"
            variant="h6"
            color="inherit"
            noWrap
            className={classes.title}
          > {nome}
          </Typography>
          <Button onClick={handleLogout}>
            <Tooltip title="LogOut">
              <ExitToAppIcon className={classes.userIcon} />
            </Tooltip>
          </Button>
        </Toolbar>
      </AppBar>
      <Hidden xsDown implementation="css">
        <Drawer
          variant="permanent"
          open
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            }),
          }}
        >
          <div className={classes.toolbar}>
            <IconLogo />
          </div>
          <MainListItems />
        </Drawer>
      </Hidden>
      <Hidden smUp implementation="css">
        <Drawer
          variant="temporary"
          anchor={theme.direction === 'rtl' ? 'right' : 'left'}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true,
          }}
        >
          <IconLogo />
          <MainListItems />
        </Drawer>
      </Hidden>
      <main className={classes.rootmain}>
        <Box xs={3} sm={3} md={3} xl={3} className={classes.containermain}>
          {props.children}
        </Box>
      </main>
    </div>
  )
}

const IconLogo = styled.div`
   background: url(${Logo}) no-repeat;
   background-size: 70%;
   width: 200px;
   height: 80px;
   margin-left: 30px;
   margin-top: 10px;
 `
