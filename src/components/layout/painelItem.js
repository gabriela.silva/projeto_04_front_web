import React from 'react'
import ListItem from '@material-ui/core/ListItem'
import Tooltip from '@material-ui/core/Tooltip'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import { makeStyles } from '@material-ui/core/styles'
import { Link } from '@reach/router'
import { useSelector } from 'react-redux'
import { Menu } from '../../views/admin'
// import { makeStyles } from '@material-ui/core/styles'
const useStyles = makeStyles((theme) => ({
  icons: {
    color: '#402672',
    marginTop: '5px',
    marginLeft: '20px',
    marginRight: '-15px'
  },
  title: {
    color: '#402672',
    marginTop: '5px',
  }
}))

const ListMenu = () => {
  const classes = useStyles()
  const tipoUsuario = useSelector((state) => state.auth.usuario.tipoUsuario)

  const rotasAutorizadas = Menu.filter((route) =>
    route.authorization.includes(tipoUsuario)
  )
  return (
    <div>
      {rotasAutorizadas.map(({ title, route, icons }, i) => (
        <ListItem button component={Link} to={'/admin' + route} key={i}>
          <Tooltip title={title}>
            <ListItemIcon className={classes.icons}>{icons}</ListItemIcon>
          </Tooltip>
          <ListItemText className={classes.title} primary={title} />
        </ListItem>
      ))}
    </div>
  )
}

export default ListMenu
