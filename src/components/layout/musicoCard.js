import { Card, CardMedia, CardContent, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { useSelector } from 'react-redux'

const useStyles = makeStyles((theme) => ({
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  cardMedia: {
    paddingTop: '56.25%' // 16:9
  },
  cardContent: {
    flexGrow: 1
  },
  body: {
    fontSize: '16px',
  },
}))
const MusicoCard = (props) => {
  const classes = useStyles()
  const musicos = useSelector((state) => state.musicos.all)

  return (
    <Card className={classes.card}>
      <CardMedia
        className={classes.cardMedia}
        // image={img}
        title="Image title"
      />
      <CardContent className={classes.cardContent}>
        <Typography className={classes.body} gutterBottom variant="h5" component="h2">
          {props.data.profile}
        </Typography>
        <Typography variant="button" display="block" gutterBottom>
          {props.data.nome}
        </Typography>
      </CardContent>
    </Card>
  )
}

export default MusicoCard
