import {
  AppBar,
  // CssBaseline,
  IconButton,
  Toolbar,
  Tooltip,
  Button
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import styled from 'styled-components'
// import BusinessCenterIcon from '@material-ui/icons/BusinessCenter'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import { Link as LinkRoute } from '@reach/router'
import Logo from '../../assets/logos/10.2.png'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    background: 'transparent',
  },
  toolbar: {
    // justifyContent: 'space-between',
  },
  logo: {
    flexGrow: 0,
  },
  space: {
    flexGrow: 3,
  },
  buttons: {
    // flexGrow: 2,
    padding: '5px',
    margin: '5px',
    color: '#402672'

  },
}))

const Header = () => {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <AppBar className={classes.appBar} position="static">
        <Toolbar className={classes.toolbar}>
          <IconLogo className={classes.logo} />
          <div className={classes.space} />
          <Button
            component={LinkRoute}
            to="signUpMusico"
            className={classes.buttons}
          >
            Seja
          </Button>
          <Button
            component={LinkRoute}
            to="signUpCliente"
            className={classes.buttons}
          >
            Contrate
          </Button>
          <Tooltip title="Login">
            <IconButton
              variant="outlined"
              size="small"
              component={LinkRoute}
              to="signin"
              className={classes.buttons}
            >
              <ExitToAppIcon />
            </IconButton>
          </Tooltip>
        </Toolbar>
      </AppBar>
    </div>
  )
}

export default Header

const IconLogo = styled.div`
   background: url(${Logo}) no-repeat;
   background-size: 100%;
   width: 150px;
   height: 64px;
   margin-top: 5px;
   padding-bottom: 90px;
 `
// toolbar: {
//   borderBottom: `1px solid ${theme.palette.divider}`,
//   background: 'transparent',
//   justifyContent: 'space-between',
//   flexFlow: 'row',
// },
// appbar: {
//   // background: '#fcd099'
//   background: 'transparent',
//   // boxShadow: 'none'
// }
// }))
