import React from 'react'
import bandImg from '../../assets/images/banda2.jpg'
import Typography from '@material-ui/core/Typography'
import { makeStyles, useTheme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  imgcontainer: {
    position: 'relative',
    backgroundSize: 'cover 90%',
    backgroundColor: 'rgba(180, 144, 199, .9)',
    width: '100%',
    height: '750px',
    overflow: 'hidden'

  },
  banner: {
    opacity: '70%',
    width: '100%',
    height: '800px'
  },
  texto: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    color: '#fff',
    fontSize: '20px',
    textStyle: 'bold',
  },
}))

function Main() {
  const classes = useStyles()
  const theme = useTheme()
  return (
    <div className={classes.root}>
      <div className={classes.imgcontainer}>
        <img src={bandImg} className={classes.banner} />
        <Typography variant="h6" className={classes.texto}>
          Tuned In é um lugar para profissionais da música poderem mostrar o seu trabalho e anunciar seus serviços.
          <hr />
          Encontre um profissional perfeito para colaboração ou contrate alguém para um gig.
        </Typography>
      </div>
    </div>
  )
}

export default Main
