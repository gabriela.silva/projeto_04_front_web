import React from 'react'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import {
  getAll as allMusicos,
  obterPost
} from '../../store/musicos/action'
import { getAll as allPosts } from '../../store/post/action'
import PostCard from '../../components/post/postCard'

import { useDispatch, useSelector } from 'react-redux'

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2)
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6)
  },
  heroButtons: {
    marginTop: theme.spacing(4)
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8)
  },
  cardmusico: {
    maxWidth: 345,
  }

}))

function Posts() {
  const classes = useStyles()
  const dispatch = useDispatch()
  const usuario = useSelector((state) => state.auth.usuario)
  const posts = useSelector((state) => state.post.all)

  const callMusicos = React.useCallback(() => {
    dispatch(allMusicos())
    dispatch(allPosts())
    dispatch(obterPost())
  }, [dispatch])

  React.useEffect(() => {
    callMusicos()
  }, [callMusicos])

  function montarPost(data) {
    const userPost = usuario.data.post
    if (userPost === data.id) {
      return (
        data.map((card, i) => (
          <Grid item key={i} xs={12} sm={6} md={3}>
            <PostCard data={card} />
          </Grid>
        ))
      )
    } else {
      return (
        <div />
      )
    }
  }

  return (
    <main>
      <Container className={classes.cardGrid}>
        <Grid container spacing={4}>
          {montarPost(posts)}
        </Grid>
      </Container>
    </main>
  )
}

export default Posts
