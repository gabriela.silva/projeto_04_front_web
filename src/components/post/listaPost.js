import React from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'

import DataList from '../datagrid/index'
import { useSelector } from 'react-redux'

const ListaPosts = ({ open, close }) => {
  const columnsPosts = [
    {
      field: 'nome',
      headerName: 'Nome',
      flex: 1,
      width: 340,
      disableColumnMenu: true
    },
    {
      field: 'categoria',
      headerName: 'Categoria',
      flex: 1,
      width: 340,
      disableColumnMenu: true
    }
  ]
  const posts = useSelector((state) => state.musicos.post)

  return (
    <Dialog
      open={open}
      onClose={close}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Lista de Posts</DialogTitle>
      <DialogContent style={{ width: '500px ' }}>
        <DataList
          data={posts}
          columns={columnsPosts}
          loading={false}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={close} color="primary" autoFocus>
          fechar
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default ListaPosts
