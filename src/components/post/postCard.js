import { Card, CardMedia, CardContent, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  cardMedia: {
    paddingTop: '56.25%'
  },
  cardContent: {
    flexGrow: 1
  },
  body: {
    fontSize: '16px',
    color: '#402672'
  },
  body2: {
    fontSize: '14px',
    color: '#402672'
  }
}))
const PostCard = (props) => {
  const classes = useStyles()

  const img = `http://localhost:3010${props.data.imagem}`

  return (
    <Card className={classes.card}>
      <CardMedia
        className={classes.cardMedia}
        image={img}
        title="Image title"
      />
      <CardContent className={classes.cardContent}>
        <Typography className={classes.body} gutterBottom>
          {props.data.mensagem}
        </Typography>
        <Typography className={classes.body2} display="block" gutterBottom>
          {props.data.categoriaNome.toUpperCase()}
        </Typography>
        <Typography display="block" className={classes.body2} gutterBottom>
          {props.data.usuarioNome.toUpperCase()}
        </Typography>
      </CardContent>
    </Card>
  )
}

export default PostCard
