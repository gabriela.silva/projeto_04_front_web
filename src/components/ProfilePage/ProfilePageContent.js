import React from 'react'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'
import AppBar from '@material-ui/core/AppBar'
import CssBaseline from '@material-ui/core/CssBaseline'
import Drawer from '@material-ui/core/Drawer'
import Hidden from '@material-ui/core/Hidden'
import IconButton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Tooltip from '@material-ui/core/Tooltip'
import MenuIcon from '@material-ui/icons/Menu'
import Toolbar from '@material-ui/core/Toolbar'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Logo from '../../assets/logos/10.2.png'
import styled from 'styled-components'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import { Link as LinkRoute } from '@reach/router'
// import teste from '../../assets/logos/11.png'

import {
  Subject as SubjectIcon,
  Search as SearchIcon,
  AccountCircle as AccountCircleIcon,
  ExitToApp as ExitToAppIcon,
} from '@material-ui/icons'

const drawerWidth = 210
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    backgroundColor: 'rgba(180, 144, 199)',
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
      flexFlow: 'row',
      justifyContent: 'flex-end',
    },
    '@media screnn and (max-width: 400px)': {
      width: `calc(90% - ${drawerWidth}px)`,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,

  list: {
    marginLeft: '25px',
    marginTop: '15px',
    color: '#402672'
  },
  drawerPaper: {
    width: drawerWidth,
  },
  exit: {
    color: 'white',
  },
}))

function ProfileHeader(props) {
  const usuario = useSelector((state) => state.auth.usuario)
  const { window } = props
  const classes = useStyles()
  const theme = useTheme()
  const [mobileOpen, setMobileOpen] = React.useState(false)

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen)
  }

  const drawer = (
    <div className={classes.toolbar}>
      <IconLogo />
      <List className={classes.list}>
        <ListItem button>
          <AccountCircleIcon />
          <ListItemText path="/musicos">
            &ensp;&ensp;Profile
          </ListItemText>
        </ListItem>
        <ListItem button component={LinkRoute} to="/posts">
          <SubjectIcon />
          <ListItemText>
            &ensp;&ensp;Posts
          </ListItemText>
        </ListItem>
        <ListItem button>
          <SearchIcon />
          <ListItemText path="/search">
            &ensp;&ensp;Search
          </ListItemText>
        </ListItem>
      </List>
    </div>
  )

  const container = window !== undefined ? () => window().document.body : undefined

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap className={classes.user}>
            {usuario.nome}
          </Typography>
          <Button
            className={classes.exit}
            component={LinkRoute}
            to="/"
          >
            <Tooltip title="LogOut">
              <ExitToAppIcon />
            </Tooltip>
          </Button>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
    </div>
  )
}

ProfileHeader.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
}

export default ProfileHeader

const IconLogo = styled.div`
   background: url(${Logo}) no-repeat;
   background-size: 70%;
   width: 200px;
   height: 80px;
   margin-left: 40px;
   margin-top: 10px;
 `
